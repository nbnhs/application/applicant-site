const path = require("path");

module.exports = [
  {
    entry: "./src/js/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public")
    }
  },
  {
    entry: "./src/js/create-account/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public/create-account")
    }
  },
  {
    entry: "./src/js/app/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public/app")
    }
  },
  {
    entry: "./src/js/app/leadership/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public/app/leadership")
    },
    node: {
      fs: "empty"
    }
  },
  {
    entry: "./src/js/app/service/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public/app/service")
    }
  },
  {
    entry: "./src/js/app/signature/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public/app/signature")
    }
  },
  {
    entry: "./src/js/app/teacher/script.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      filename: "script.js",
      path: path.resolve(__dirname, "public/app/teacher")
    }
  }
];
