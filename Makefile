all: clean yarn script html

clean:
	@rm -rf public || mkdir public

build: script html

yarn:
	@yarn install --production=false

script: yarn
	@npx webpack-cli

html: yarn
	@cp -rf src/html/. public/
	@npx grunt

test: all
	( \
		cd public; \
		python3 -m http.server; \
	)
