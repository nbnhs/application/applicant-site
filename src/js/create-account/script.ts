import Applicant from "../applicant";
import { SUPPORT_EMAIL } from "../vars";

window.onload = async () => {
  document.getElementById("btn-submit").addEventListener("click", async () => {
    const getInput = (elementID: string) => {
      return (document.getElementById(elementID) as HTMLInputElement);
    };

    const id = getInput("id-card-number").value;
    const applicant = new Applicant(id);
    const success: boolean = await applicant.create(
      getInput("first-name").value,
      getInput("last-name").value,
      getInput("email").value
    );

    if (success) {
      alert(
        "Your account has been registered! You will now be redirected to the login screen."
      );
      window.location.replace("../index.html");
    } else {
      alert(`We couldn't create your account. Have you already registered for an account, and were you invited to make one? Try logging in at app.nbnhs.org with your ID card number. If this issue persists, contact ${SUPPORT_EMAIL}.`);
      for (const inputName of ["id-card-number", "first-name", "last-name", "email"]) {
        getInput(inputName).value = "";
      }
    }
  });
};
