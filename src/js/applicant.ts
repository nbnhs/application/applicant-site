import { RELAY_DOMAIN } from "./vars";

/**
 * Represents a database entry of kind "Applicant" in Google Cloud Datastore.
 */
interface IDatabaseEntry {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  leadership: string;
  service: string;
  teacher: string;
  signature: string;
}

interface ICreatedApplicant {
  first_name: string;
  last_name: string;
  email: string;
  leadership: string;
  service: string;
  teacher: string;
  signature: string;
}

/**
 * Represents an Applicant looking to join the NHS. Contains methods needed to interface with databases.
 */
export default class Applicant {
  // represents properties of the GCloud Datastore entry
  public static validQueries = [
    "signature",
    "leadership",
    "service",
    "teacher"
  ];

  /**
   * Gets all properties of the given Applicant from the database.
   * @param ID - Applicant ID card number.
   * @returns A (promise for an) database entry object conforming to IDatabaseEntry.
   */
  public static async getDatabaseEntryProperties(
    ID: string
  ): Promise<IDatabaseEntry> {
    console.log(`Getting database entry for ${ID}`);
    const url = `https://${RELAY_DOMAIN}?operation=get&id=${ID}`;

    console.log("Making request...");
    const response = await fetch(url);
    console.log("Request made.");

    if (response.ok) {
      return await response.json(); // returns object, which is converted to IDatabaseEntry
    } else {
      throw new Error(`Invalid response! Couldn't get ${ID} from database.`);
    }
  }

  /**
   * Sets all properties of the given Applicant in the database to the ones provided.
   * @param ID - Applicant ID card number.
   * @param properties - Object containing properties (in compliance with IDatabaseEntry).
   * @returns A (promise for an) boolean (true upon success).
   */
  public static async setDatabaseEntryProperties(
    ID: string,
    properties: IDatabaseEntry | ICreatedApplicant
  ): Promise<boolean> {
    const url = `https://${RELAY_DOMAIN}?operation=set&id=${ID}&properties=${JSON.stringify(
      properties
    )}`;
    const response = await fetch(url);

    return response.ok;
  }

  constructor(public id: string) {} // automatically does `this.id = id`

  /**
   * Creates an applicant in the Cloud Datastore, provided that said applicant is on the list of invited applicants.
   * @returns Boolean value corresponding to the success or failure of the operation.
   */
  public async create(first: string, last: string, email: string) {
    const properties: ICreatedApplicant = {
      first_name: first,
      last_name: last,
      email,
      service: "Incomplete",
      leadership: "Incomplete",
      signature: "Incomplete",
      teacher: "Incomplete"
    };

    const success = await Applicant.setDatabaseEntryProperties(
      this.id,
      properties
    );
    if (success) {
      const url = `https://${RELAY_DOMAIN}?operation=sendconfirmation&id=${this.id}`;
      fetch(url);
    }
    return success;
  }

  /**
   * Gets the name of Applicant.
   * @returns A (promise for an) array containing the Applicant's name as ["first", "last"].
   */
  public async name(): Promise<string[]> {
    const data = await Applicant.getDatabaseEntryProperties(this.id);
    return [data.first_name, data.last_name];
  }

  /**
   * Retrieves a single proprerty of the Applicant from the database.
   * @param name - Name of the property to get.
   * @returns A (promise for a) string containing the value of the property.
   */
  public async getPropertyFromDatabase(name: string): Promise<string> {
    const json = await Applicant.getDatabaseEntryProperties(this.id);
    return json[name];
  }

  /**
   * Sets a single property of the Applicant in the database.
   * @param name - Name of the property to set.
   * @param val - Value of the property to be set.
   * @returns A (promise for a) boolean (true upon success).
   */
  public async setPropertyInDatabase(
    name: string,
    val: string
  ): Promise<boolean> {
    const json = await Applicant.getDatabaseEntryProperties(this.id);
    json[name] = val;
    return await Applicant.setDatabaseEntryProperties(this.id, json);
  }
}
