import { SUPPORT_EMAIL } from "../vars";

/**
 * Retrieves a cookie from "document.cookie".
 * Thank you to {@link https://gist.github.com/hunan-rostomyan/28e8702c1cecff41f7fe64345b76f2ca | this guy}!
 * @param name - name of the cookie to retrieve
 */
export default function getCookie(name: string): string {
  const nameLenPlus = name.length + 1;
  const result: any = document.cookie
    .split(";")
    .map(c => c.trim())
    .filter(cookie => {
      return cookie.substring(0, nameLenPlus) === `${name}=`;
    })
    .map(cookie => {
      return decodeURIComponent(cookie.substring(nameLenPlus));
    })[0];

  if (result == undefined || result == null) {
    alert(
      `Your session has expired. Please login again. If this issue persists contact ${SUPPORT_EMAIL}`
    );
    window.location.replace("../index.html");
    return null;
  }

  return result as string;
}
