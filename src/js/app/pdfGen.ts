import * as jsPDF from "jspdf";

/**
 * A text message for inclusion in a jsPDF.
 */
export class PDFText {
  constructor(
    public text: string,
    public posX: number, // x-pos in document
    public posY: number, // y-pos in document
    public fontSize: number
  ) {}
}

/**
 * An image for inclusion in a jsPDF.
 */
export class PDFImage {
  constructor(
    public data: any,
    public format: string, // PNG/JPEG/etc.
    public posX: number, // x-pos in document
    public posY: number, // y-pos in document
    public width: number,
    public height: number
  ) {}
}

/**
 * Adds a new event listener to generate a PDF when the user clicks any buttons of class "btn-generate".
 * @param pdfname - the name of the pdf that will be saved to the applicant computer (sans .pdf)
 * @param window - the current page's window object
 * @param applicantID - ID of the Applicant for whom the PDF is being generated
 * @param text - array of text messages to attach to the PDF
 * @param images - array of images to attach to the PDF
 */
export function generatePDF(
  pdfname: string,
  window: Window,
  applicantID: string,
  text: PDFText[],
  images: PDFImage[]
) {
  window.onload = () => {
    document.querySelector("#btn-generate").addEventListener("click", () => {
      const doc = new jsPDF();

      doc.setFont("helvetica");
      doc.setFontSize(15);

      doc.text(`Applicant ID: ${applicantID}`, 7, 12);
      text.forEach((element) => {
        const txt = doc.splitTextToSize(element.text, 180); // enables text wrapping
        doc.setFontSize(element.fontSize);
        doc.text(txt, element.posX, element.posY);
      });
      images.forEach((element) => {
        doc.addImage(
          element.data,
          element.format,
          element.posX,
          element.posY,
          element.width,
          element.height
        );
      });

      doc.save(`${pdfname}.pdf`);
    });
  };
}
