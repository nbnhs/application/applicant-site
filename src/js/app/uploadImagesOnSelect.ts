import { RELAY_DOMAIN } from "../vars";
import getCookie from "./getCookie";

export async function uploadJSON(ID: string, data: any) {
  return await uploadImageFile("application/json", "leadership", JSON.stringify(data), ID);
}

/**
 * Uploads an image to the database with a POST request, attached to a certain Applicant.
 * @param format - format of the image (e.g. image/png)
 * @param sender - sender of the POST request, determining where in the database the image will land
 * @param imageData - the actual image (should be a blob of some kind)
 * @param ID - ID number of the Applicant to attach the image to.
 */
async function uploadImageFile(
  format: string,
  sender: string,
  imageData: any,
  ID: string
): Promise<string> {
  console.log("Uploading image...");
  const url = `https://${RELAY_DOMAIN}`;
  const customHeaders = new Headers();
  customHeaders.append("Content-Type", format);
  customHeaders.append("Content-Length", String(imageData.length));

  // convey information to the relay
  customHeaders.append("X-NHS-ID", ID);
  customHeaders.append("X-NHS-ImageKind", sender);

  const response = await fetch(url, {
    method: "POST",
    headers: customHeaders,
    body: imageData
  });

  console.log("Uploaded.");
  if (!response.ok) {
    throw new Error(`Invalid response from database: Unable to upload image for ${ID}`);
  }
  return await response.text();
}

/**
 * Creates a new event listener for all buttons of class "btn-uploadsubmit" to upload the user-selected image.
 * @param document - the document object of the current page
 * @param window - the window object of the current page
 * @param kind - kind of image being uploaded; see {@link ../applicant | Applicant.validQueries}.
 */
export default function uploadImagesOnSelect(
  document: Document,
  window: Window,
  kind: string
): void {
  document.querySelector("#btn-uploadsubmit").addEventListener("click", () => {
    // Changes button to display "Please wait" instead of "Submit"
    (document.querySelector(
      "#btn-uploadsubmit"
    ) as HTMLButtonElement).innerText = "Please wait...";

    const input = (document.getElementById("upload") as HTMLInputElement)
      .files[0];
    const id = getCookie("id");

    uploadImageFile(input.type, kind, input, id).then((val: string) => {
      console.log(val);
      alert("Success!");
      window.location.replace("../index.html");
    });
  });
}
