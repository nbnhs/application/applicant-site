import { SUPPORT_EMAIL } from "../../vars";
import getCookie from "../getCookie";
import { uploadJSON } from "../uploadImagesOnSelect";

// leadership property in the Applicants in the GCloud Datastore is nothing but an array of these.
interface ILeadershipEntry {
  yearsInvolved: string[];
  description: string;
}

window.onload = () => {
  document.querySelector("#btn-submit").addEventListener("click", () => {
    // Changes button to display "Please wait" instead of "Submit"
    (document.querySelector("#btn-submit") as HTMLButtonElement).innerText =
      "Please wait...";

    const activities: ILeadershipEntry[] = []; // to be uploaded

    for (const num of [1, 2, 3]) {
      // Checkboxes asking the user if they participated in the activity in xth year
      const ninth = document.getElementById(`9th-${num}`) as HTMLInputElement;
      const tenth = document.getElementById(`10th-${num}`) as HTMLInputElement;
      const eleventh = document.getElementById(
        `11th-${num}`
      ) as HTMLInputElement;
      const twelfth = document.getElementById(
        `12th-${num}`
      ) as HTMLInputElement;

      const years: string[] = [];
      [ninth, tenth, eleventh, twelfth].forEach((val: HTMLInputElement) => {
        if (val.checked) {
          // if they checked the checkbox
          years.push(val.value); // either 9th, 10th, 11th, or 12th
        }
      });

      const desc = (document.getElementById(
        `desc-${num}`
      ) as HTMLTextAreaElement).value;
      activities.push({ yearsInvolved: years, description: desc });
    }

    uploadJSON(getCookie("id"), activities)
      .then(_ => {
        // early navigation away from page disrupts GET request; necessary to wait until it finishes
        alert("Submitted successfully. Thank you!");
        window.location.replace("../index.html");
      })
      .catch((failure: string) => {
        alert(
          `Couldn't upload information. Got error ${failure}. Try again or email ${SUPPORT_EMAIL} if this persists.`
        );
      });
  });
};
