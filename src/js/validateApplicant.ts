import Applicant from "./applicant";

/**
 * Validates the existence of an Applicant in the database.
 *
 * @remarks
 * This function relies on the {@link applicant | Applicant class} to fetch info from the database.
 *
 * @param id - The ID card number of the Applicant.
 * @returns A boolean (Promise) which will be true if the Applicant ID is valid.
 */
export default async function validateApplicant(id: string): Promise<boolean> {
  console.log("Validating ID...");
  await Applicant.getDatabaseEntryProperties(id); // throws error if ID is wrong, so success means ID is right
  console.log("Validated.");
  return true;
}
